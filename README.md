# Vagrant VMware Workstation - Ubuntu 22.04

## Requirements

1. Install [Vagrant](https://developer.hashicorp.com/vagrant/downloads)
2. Install Plugin `vagrant-vmware-workstation`:

    ```bash
    vagrant plugin install vagrant-vmware-workstation
    ```

3. Install License for VMware Workstation Pro

    ```bash
    vagrant plugin license vagrant-vmware-workstation <license.lic>
    ```

4. Start Vagrant

    ```bash
    vagrant up --provider=vmware_workstation
    ```

5. Login to VM

    ```bash
    vagrant ssh
    ```
